package fft

import assertEquals
import math.Complex
import kotlin.test.Test

class FFTTest {
    @Test
    fun `the fourier transform with size 1 should be the identity`() {
        assertEquals(
            expected = listOf(Complex(1.0, 0.0)),
            actual = FFTProcessor.process(listOf(Complex(1.0, 0.0)))
        )
    }

    @Test
    fun `the fourier transform of 1 1 should be 2 0`() {
        assertEquals(
            expected = listOf(
                Complex(2.0, 0.0),
                Complex(0.0, 0.0)
            ),
            actual = FFTProcessor.process(
                listOf(
                    Complex(1.0, 0.0),
                    Complex(1.0, 0.0)
                )
            )
        )
    }

    @Test
    fun `the fourier transform of 1 1 1 1 should be 4 0 0 0`() {
        assertEquals(
            expected = listOf(
                Complex(4.0, 0.0),
                Complex(0.0, 0.0),
                Complex(0.0, 0.0),
                Complex(0.0, 0.0)
            ),
            actual = FFTProcessor.process(
                listOf(
                    Complex(1.0, 0.0),
                    Complex(1.0, 0.0),
                    Complex(1.0, 0.0),
                    Complex(1.0, 0.0)
                )
            )
        )
    }

    @Test
    fun `the fourier transform of 1 1 1 0 should be 3 -i 1 i`() {
        assertEquals(
            expected = listOf(
                Complex(3.0, 0.0),
                Complex(0.0, -1.0),
                Complex(1.0, 0.0),
                Complex(0.0, 1.0)
            ),
            actual = FFTProcessor.process(
                listOf(
                    Complex(1.0, 0.0),
                    Complex(1.0, 0.0),
                    Complex(1.0, 0.0),
                    Complex(0.0, 0.0)
                )
            )
        )
    }

    @Test
    fun `the fourier transform should be correct on random inputs of length 8`() {
        assertEquals(
            expected = listOf(
                Complex(42.0, 0.0),
                Complex(0.121, -2.879),
                Complex(-1.0, -11.0),
                Complex(-4.121, 7.121),
                Complex(-16.0, 0.0),
                Complex(-4.121, -7.121),
                Complex(-1.0, 11.0),
                Complex(0.121, 2.879)),
            actual = FFTProcessor.process(
                listOf(
                    Complex(2.0, 0.0),
                    Complex(10.0, 0.0),
                    Complex(6.0, 0.0),
                    Complex(3.0, 0.0),
                    Complex(4.0, 0.0),
                    Complex(10.0, 0.0),
                    Complex(1.0, 0.0),
                    Complex(6.0, 0.0)
                )
            ),
            absoluteTolerance = 0.01
        )
    }
}