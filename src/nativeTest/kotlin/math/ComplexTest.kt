package math

import assertEquals
import kotlin.test.Test

class ComplexTest {
    @Test
    fun `the division should yield correct results`() {
        assertEquals(
            expected = Complex(31.0/74, 1.0/74) ,
            actual = Complex(2.0, 3.0) / Complex (5.0, 7.0)
        )
    }
}