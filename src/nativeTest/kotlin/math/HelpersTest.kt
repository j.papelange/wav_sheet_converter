package math

import assertEquals
import kotlin.test.Test

class HelpersTest {
    @Test
    fun `the first root of unity should be 1`() {
        assertEquals(expected = Complex(1.0, 0.0), actual = nthRootOfUnity(1))
    }

    @Test
    fun `the second root of unity should be -1`() {
        assertEquals(expected = Complex(-1.0, 0.0), actual = nthRootOfUnity(2))
    }

    @Test
    fun `the fourth root of unity should be i`() {
        assertEquals(expected = Complex(0.0, 1.0), actual = nthRootOfUnity(4))
    }


    @Test
    fun `the first negative root of unity should be 1`() {
        assertEquals(expected = Complex(1.0, 0.0), actual = nthRootOfUnityNegative(1))
    }

    @Test
    fun `the second negative root of unity should be -1`() {
        assertEquals(expected = Complex(-1.0, 0.0), actual = nthRootOfUnityNegative(2))
    }

    @Test
    fun `the fourth negative root of unity should be i`() {
        assertEquals(expected = Complex(0.0, -1.0), actual = nthRootOfUnityNegative(4))
    }

    @Test
    fun `the first roots of unity should be 1`() =
        assertEquals(expected = listOf(Complex(1.0, 0.0)), actual = createNthRootsOfUnity(1))

    @Test
    fun `the second roots of unity should be 1 and -1`() {
        assertEquals(expected = listOf(Complex(1.0, 0.0), Complex(-1.0, 0.0)), actual = createNthRootsOfUnity(2))
    }

    @Test
    fun `the fourth roots of unity should be 1 -i -1 and i`() {
        assertEquals(
            expected = listOf(Complex(1.0, 0.0), Complex(0.0, -1.0), Complex(-1.0, 0.0), Complex(0.0, 1.0)),
            actual = createNthRootsOfUnity(4)
        )
    }
}