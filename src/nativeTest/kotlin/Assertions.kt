import math.Complex
import kotlin.test.assertTrue

fun assertEquals(expected: Complex, actual: Complex, absoluteTolerance: Double = 0.00001, message: String? = null) {
    assertTrue(
        (actual - expected).absoluteValue() < absoluteTolerance, message ?: "Expected <$expected>, actual <$actual>."
    )
}

fun assertEquals(expected: List<Complex>, actual: List<Complex>, absoluteTolerance: Double = 0.00001, message: String? = null) {
    kotlin.test.assertEquals (expected.size, actual.size, message ?: "Expected <${actual}> to have size <${expected.size}> instead of <${actual.size}>.")
    for (id in actual.indices) assertEquals(expected[id], actual[id], absoluteTolerance, message ?: "Expected <${expected[id]}>, actual <${actual[id]}> at index $id.")
}