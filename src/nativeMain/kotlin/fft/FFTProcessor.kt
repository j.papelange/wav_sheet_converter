package fft

import math.Complex
import math.createNthRootsOfUnity

object FFTProcessor {
    fun process(input: List<Complex>): List<Complex> {
        checkFFTRequirements(input)
        return fourierTransform(input)
    }

    private fun checkFFTRequirements(input: List<Complex>) {
        val n = input.size
        require(n and (n - 1) == 0) { "FFT requires a power of two." }
    }

    private fun fourierTransform(input: List<Complex>): List<Complex> {
        val n = input.size

        if (n <= 1) return listOf(input.first())

        val rootNList = createNthRootsOfUnity(n)
        val rootNListHalf = rootNList.filterIndexed { id, _ -> id < rootNList.size / 2 }

        val inputEven = input.filterIndexed { id, _ -> id and 1 == 0 }
        val inputOdd = input.filterIndexed { id, _ -> id and 1 == 1 }

        val fftEven = fourierTransform(inputEven)
        val fftOdd = fourierTransform(inputOdd)
        val fftOddAdjusted = fftOdd.zip(rootNListHalf) { odd, root -> odd * root }

        val fftFirstHalf = fftEven.zip(fftOddAdjusted) { even, odd -> even + odd }
        val fftSecondHalf = fftEven.zip(fftOddAdjusted) { even, odd -> even - odd }

        return fftFirstHalf + fftSecondHalf
    }
}




