import fft.FFTProcessor
import math.toComplex
import tone.*
import wav.WAV

fun main() {
    val note = Note(Pitch.A, 4)

    val triadWAV = WAV(5.0, 16, 44100, 1)

    triadWAV.addTone(Tone.SineTone, note, 0.1, 0.0, 5.0)
    triadWAV.addTone(Tone.SineTone, note.addSemiTones(3).addSemiTones(), 0.1, 1.0, 4.0)
    triadWAV.addTone(Tone.SineTone, note.addSemiTones(8).subtractSemiTones(), 0.1, 2.0, 3.0)

    triadWAV.writeToFile("triad.wav")

    val vibratoWAV = WAV(5.0, 16, 44100, 1)

    vibratoWAV.addTone(
        Tone.SineTone,
        note,
        0.2,
        0.0,
        4.0,
        listOf<Modifier>(FrequencyModifier(FrequencyModifierType.SineVibrato, 0.002, 2.0)),
        0.1
    )

    vibratoWAV.writeToFile("vibrato.wav")

    val fadeWAV = WAV(5.0, 16, 44100, 1)

    fadeWAV.addTone(
        Tone.SineTone, note, 0.2, 0.0, 5.0, listOf<Modifier>(
            AmplitudeModifier(AmplitudeModifierType.LinFadeIn, 1.0, 0.0, 1.0),
            AmplitudeModifier(AmplitudeModifierType.LinFadeOut, 2.0, 0.0, 1.0)
        ), 0.1
    )

    fadeWAV.writeToFile("fade.wav")

    val overtoneWAV = WAV(5.0, 16, 44100, 1)

    overtoneWAV.addTone(
        Tone.SineTone,
        note,
        0.2,
        0.0,
        2.0,
        listOf<Modifier>(OvertoneModifier(listOf(1.0, 0.5, 0.2))),
        0.1
    )

    overtoneWAV.addTone(Tone.SineTone, note, 0.2, 3.0, 2.0)
    overtoneWAV.addTone(
        Tone.SineTone, note.addSemiTones(PitchIntervals.Octave.distance), 0.1, 3.0, 2.0
    )
    overtoneWAV.addTone(
        Tone.SineTone,
        note.addSemiTones(PitchIntervals.Octave.distance).addSemiTones(PitchIntervals.Fifth.distance),
        0.04,
        3.0,
        2.0
    )

    overtoneWAV.writeToFile("overtone.wav")

    println(PitchIntervals.values()[1].distance)

    val simpleWAV = WAV(5.0, 16, 44100, 1)

    simpleWAV.addTone(Tone.SineTone, Note(Pitch.A, 3), 0.1, 0.0, 5.0)

    simpleWAV.writeToFile("simple.wav")

    val samples = simpleWAV.getSample().filterIndexed{ id, _ -> id < 32768 }

    val fftSamples = FFTProcessor.process(samples.map { it.toComplex() })

    var max = 0.0
    for (i in samples.indices) {
        if (max < fftSamples[i].absoluteValue()) max = fftSamples[i].absoluteValue()
    }

    for (i in samples.indices) {
        if (max - fftSamples[i].absoluteValue() < 1.0) println("$i -> ${fftSamples[i]}")
    }
}