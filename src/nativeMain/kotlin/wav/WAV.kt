package wav

import bitmanipulation.toBytes
import kotlinx.cinterop.toCValues
import platform.posix.fclose
import platform.posix.fopen
import platform.posix.fwrite
import tone.*

data class WAVHeader(
    private val numSamples: Int,
    private val bitsPerSample: Short,
    private val sampleRate: Int,
    private val numChannels: Short
) {
    private val byteRate: Int = sampleRate * numChannels * bitsPerSample / 8
    private val blockAlign: Short = (numChannels * bitsPerSample / 8).toShort()
    private val subchunk2Size: Int = numSamples * numChannels * bitsPerSample / 8
    private val chunkSize: Int = 4 + (8 + FMT_CHUNK_SIZE) + (8 + subchunk2Size)

    private val data = listOf(
        CHUNK_ID,
        chunkSize,
        FORMAT,
        FMT_CHUNK_ID,
        FMT_CHUNK_SIZE,
        AUDIO_FORMAT,
        numChannels,
        sampleRate,
        byteRate,
        blockAlign,
        bitsPerSample,
        DATA_CHUNK_ID,
        subchunk2Size
    )

    fun toByteArray(): ByteArray {
        return data.flatMap { i -> i.toBytes() }.toByteArray()
    }

    companion object {
        private const val CHUNK_ID = "RIFF"
        private const val FORMAT = "WAVE"
        private const val FMT_CHUNK_ID = "fmt "
        private const val DATA_CHUNK_ID = "data"
        private const val FMT_CHUNK_SIZE: Int = 16 // size of the remaining fmt sub-chunk (for PCM)
        private const val AUDIO_FORMAT: Short = 1 // uncompressed Pulse-Code-Modulation
    }
}

class WAV(
    duration: Double, private val bitsPerSample: Short, private val sampleRate: Int, private val numChannels: Short
) {
    private val samples = Array((duration * sampleRate).toInt()) { ZERO }

    fun getSample() = samples

    private fun toByteArray(): ByteArray {
        val header = WAVHeader(samples.size, bitsPerSample, sampleRate, numChannels).toByteArray()
        val content = samples.flatMap { i -> i.toBytes() }.toByteArray()
        return header + content
    }

    fun writeToFile(filename: String) {
        // open writable binary file
        val filePointer = fopen(filename, "wb") ?: throw NullPointerException("Cannot open file $filename!")

        val wavArray = this.toByteArray()
        fwrite(wavArray.toCValues(), 1, wavArray.size.toULong(), filePointer)

        fclose(filePointer)
    }

    // TODO currently only for one channel and 16-bit samples
    fun addTone(
        baseTone: Tone,
        baseNote: Note,
        relAmplitude: Double,
        start: Double,
        duration: Double,
        modifiers: List<Modifier> = emptyList(),
        offset: Double = 0.0
    ) {
        val amplitude = relAmplitude * Short.MAX_VALUE
        val amplitudeModifier = modifiers.filterIsInstance<AmplitudeModifier>()
        val frequencyModifier = modifiers.filterIsInstance<FrequencyModifier>()
        val overtoneModifiers = modifiers.filterIsInstance<OvertoneModifier>()


        val toneSamples = createTone(
            baseTone,
            baseNote.frequency,
            amplitude,
            duration,
            sampleRate,
            overtoneModifiers,
            frequencyModifier,
            amplitudeModifier,
            offset
        )

        toneSamples.forEachIndexed { index, noteAmplitude ->
            if ((start * sampleRate).toInt() + index < samples.size) samples[(start * sampleRate).toInt() + index] =
                (samples[(start * sampleRate).toInt() + index] + noteAmplitude).toShort()
        }
    }

    companion object {
        private const val ZERO: Short = 0
    }
}