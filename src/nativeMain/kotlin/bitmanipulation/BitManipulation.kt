package bitmanipulation

fun stringToBytes(value: String): List<Byte> =
    (value.indices).map { index -> value[index].code.toByte()}

fun numberToBytes(value: Number): List<Byte> {
    return when (value) {
        is Byte -> numberToBytes(Byte.SIZE_BYTES, value)
        is Short -> numberToBytes(Short.SIZE_BYTES, value)
        is Int -> numberToBytes(Int.SIZE_BYTES, value)
        is Long -> numberToBytes(Long.SIZE_BYTES, value)

        else -> throw IllegalArgumentException()
    }
}

fun Comparable<*>.toBytes(): List<Byte> {
    return when (this) {
        is Number -> numberToBytes(this)
        is String -> stringToBytes(this)

        else -> throw IllegalArgumentException()
    }
}

fun numberToBytes(sizeBytes: Int, number: Number): List<Byte> =
    (0 until sizeBytes).map { index -> (number.toLong() shr (index * 8)).toByte() }
