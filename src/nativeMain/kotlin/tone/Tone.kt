package tone

import kotlin.math.sin
import kotlin.math.PI


enum class Tone(val block: (Double, Double, Double) -> Double) {
    SineTone({ amplitude, frequency, time -> amplitude * sin(2.0 * PI * frequency * time) })
}

fun createTone(
    baseTone: Tone,
    frequency: Double,
    amplitude: Double,
    duration: Double,
    sampleRate: Int,
    overtoneModifiers: List<OvertoneModifier>,
    frequencyModifiers: List<FrequencyModifier>,
    amplitudeModifiers: List<AmplitudeModifier>,
    offset: Double = 0.0
): List<Long> {
    val sampleNumber = (duration * sampleRate).toInt()
    val realSampleTimes = List(sampleNumber) { offset + it.toDouble() / sampleRate }

    overtoneModifiers.forEach { modifier ->
        var samples = List(sampleNumber) { 0L }

        modifier.overtonePattern.forEachIndexed { overtoneNumber, overtoneAmplitude ->
            val newSamples = createTone(
                baseTone,
                frequency * (overtoneNumber + 1),
                amplitude * overtoneAmplitude,
                duration,
                sampleRate,
                overtoneModifiers - modifier,
                frequencyModifiers,
                amplitudeModifiers,
                offset
            )

            samples = samples.zip(newSamples) { first, second -> first + second }
        }

        return samples
    }

    var adjustedSampleTimes = realSampleTimes

    frequencyModifiers.forEach { currentModifier ->
        adjustedSampleTimes =
            adjustedSampleTimes.zip(realSampleTimes.map { currentModifier.apply(it) }) { time, distortion -> time + distortion }
    }

    var samples = List(sampleNumber) {
        baseTone.block(amplitude, frequency, adjustedSampleTimes[it])
    }

    amplitudeModifiers.forEach { currentModifier ->
        currentModifier.setEndTime(duration)
        samples =
            samples.zip(realSampleTimes.map { currentModifier.apply(it) }) { amplitude, multiplier -> amplitude * multiplier }
    }

    return samples.map { it.toLong() }
}

interface Modifier

data class OvertoneModifier(
    val overtonePattern: List<Double>
) : Modifier

enum class FrequencyModifierType {
    SineVibrato
}

class FrequencyModifier(
    private val type: FrequencyModifierType,
    private val strength: Double = 1.0,
    private val frequency: Double = 1.0
) : Modifier {
    fun apply(realTime: Double): Double {
        return when (type) {
            FrequencyModifierType.SineVibrato -> strength / frequency * sin(2.0 * PI * frequency * realTime)
        }

    }
}

enum class AmplitudeModifierType {
    LinFadeIn, LinFadeOut
}

class AmplitudeModifier(
    private val type: AmplitudeModifierType,
    private val duration: Double = 0.0,
    private val amplitudeOuter: Double = 0.0,
    private val amplitudeInner: Double = 1.0
) : Modifier {
    private val amplitudeDifference = amplitudeInner - amplitudeOuter
    private var endTime: Double = 0.0
    fun setEndTime(time: Double) {
        endTime = time
    }

    fun apply(realTime: Double): Double {
        return when (type) {
            AmplitudeModifierType.LinFadeIn -> {
                when {
                    realTime < STARTING_TIME -> amplitudeOuter
                    realTime > duration -> amplitudeInner
                    else -> amplitudeOuter + amplitudeDifference * (realTime - STARTING_TIME) / duration
                }
            }

            AmplitudeModifierType.LinFadeOut -> {
                when {
                    realTime > endTime -> amplitudeOuter
                    realTime < endTime - duration -> amplitudeInner
                    else -> amplitudeOuter + amplitudeDifference * (endTime - realTime) / duration
                }
            }
        }
    }

    companion object {
        private const val STARTING_TIME = 0.0
    }
}