package tone

import kotlin.math.pow

// Frequencies of different tones in the sub-contra octave with equal temperament
enum class Pitch(val frequency: Double) {
    C(16.35),
    CSharp(17.32),
    D(18.35),
    DSharp(19.45),
    E(20.60),
    F(21.83),
    FSharp(23.12),
    G(24.50),
    GSharp(25.96),
    A(27.50),
    ASharp(29.14),
    B(30.87),
}

enum class PitchIntervals(val distance: Int) {
    Unison(0),
    MinorSecond(1),
    MajorSecond(2),
    MinorThird(3),
    MajorThird(4),
    Fourth(5),
    Tritone(6),
    Fifth(7),
    MinorSixths(8),
    MajorSixths(9),
    MinorSeventh(10),
    MajorSeventh(11),
    Octave(12)
}

data class Note(private val pitch: Pitch, private val octave: Int) {
    val frequency = pitch.frequency * 2.0.pow(octave)
    private val numberPitches = Pitch.values().size

    fun addSemiTones(numSemiTones: Int = 1): Note {
        val newPitchOrdinalRaw = pitch.ordinal + numSemiTones

        val newPitch = Pitch.values()[newPitchOrdinalRaw.mod(numberPitches)]
        val newOctave = octave + (newPitchOrdinalRaw / numberPitches)

        return Note(newPitch, newOctave)
    }

    fun subtractSemiTones(numSemiTones: Int = 1): Note {
        return Note(pitch, octave - (numSemiTones / numberPitches) - 1).apply {
            addSemiTones(numberPitches - numSemiTones.mod(numberPitches))
        }
    }
}