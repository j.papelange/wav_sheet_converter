package math

import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.log2
import kotlin.math.sin

fun nthRootOfUnity(n: Int) = Complex(cos(2.0 * PI / n), sin(2.0 * PI / n))
fun nthRootOfUnityNegative(n: Int) = Complex(cos(-2.0 * PI / n), sin(-2.0 * PI / n))

// TODO currently only works on powers of two.
fun createNthRootsOfUnity(n: Int): List<Complex> {
    require(n and (n - 1) == 0) { "Power of two required." }

    var rootMList = listOf(Complex(1.0))
    repeat(log2(n.toDouble() + 0.5).toInt()) {
        val rootM = nthRootOfUnityNegative(rootMList.size * 2)
        rootMList = rootMList.flatMap { listOf(it, it * rootM) }
    }
    return rootMList
}