package math

fun Short.toComplex() = Complex(this.toDouble(), 0.0)

data class Complex(
    private val real: Double = 0.0,
    private val imaginary: Double = 0.0
) {
    operator fun plus(addend: Complex) = Complex(real + addend.real, imaginary + addend.imaginary)
    operator fun minus(subtrahend: Complex) = Complex(real - subtrahend.real, imaginary - subtrahend.imaginary)
    operator fun times(factor: Complex) =
        Complex(real * factor.real - imaginary * factor.imaginary, imaginary * factor.real + real * factor.imaginary)

    operator fun div(divisor: Complex) = divisor.let {
        val absoluteValue = it.absoluteValue()
        Complex(
            (real * divisor.real + imaginary * divisor.imaginary) / (absoluteValue),
            (imaginary * divisor.real - real * divisor.imaginary) / (absoluteValue)
        )
    }

    override fun toString(): String {
        return when {
            this.absoluteValue() < EQUALITY_THRESHOLD -> "0"
            real * real < EQUALITY_THRESHOLD -> "${imaginary}i"
            imaginary * imaginary < EQUALITY_THRESHOLD -> "$real"
            imaginary > 0 -> "$real+${imaginary}i"
            else -> "$real-${-imaginary}i"
        }
    }

    fun absoluteValue() = real * real + imaginary * imaginary

    companion object {
        private const val EQUALITY_THRESHOLD = 0.0000001
    }
}
